from django.conf.urls.defaults import *
from django.contrib import admin

from django.conf import settings

from belleville.feeds import LatestPythonEntries
from belleville.feeds import LatestDjangoEntries
from belleville.feeds import LatestEntries

admin.autodiscover()

feeds = {
    'python': LatestPythonEntries, 
    'django': LatestDjangoEntries, 
    'all': LatestEntries, 
}

urlpatterns = patterns('',
    (r'^post/', include('belleville.entry.urls')),
    (r'^admin/', include(admin.site.urls)),
    (r'^pages/', include('belleville.pages.urls')),
    (r'', include('belleville.entry.urls')),
    (r'^site_media/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.MEDIA_ROOT}),
    (r'^feeds/(?P<url>.*)/$', 'django.contrib.syndication.views.feed', {'feed_dict': feeds}),
)

