from django.contrib import admin
from django.contrib.auth.models import User, Group

from belleville.entry.models import Post, Category
from belleville.entry.admin import PostAdmin, CategoryAdmin

class AdminSite(admin.AdminSite):
    pass

site = AdminSite()

site.register(User)
site.register(Group)

site.register(Post, PostAdmin)
site.register(Category, CategoryAdmin)

