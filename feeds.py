from django.contrib.syndication.feeds import Feed

from belleville.entry.models import Post, PostCategory

class LatestDjangoEntries(Feed):
    title = "Latest Django Posts"
    link = "http://www.mydomain.com/"
    description = "Latest posts about Django"

    def items(self):
        return Post.objects.filter(postcategory__category=3, publish=True)
                           .order_by('-created_at')[:5]

class LatestPythonEntries(Feed):
    title = "Latest Python Posts"
    link = "http://www.mydomain.com/"
    description = "Latest posts about Python"

    def items(self):
        return Post.objects.filter(postcategory__category=2, publish=True)
                           .order_by('-created_at')[:5]

class LatestEntries(Feed):
    title = "My Site Title"
    link = "http://www.mydomain.com/"
    description = "Latest posts on My Site"

    def items(self):
        return Post.objects.filter(publish=True).order_by('-created_at')[:5]


