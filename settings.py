import os, logging, sys

# Django settings for belleville project.

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ADMINS = (
    ('myname', 'me@email.com'),
)
ADMIN_EMAIL = 'me@email.com'
MANAGERS = ADMINS
DEFAULT_FROM_EMAIL = 'me@email.com'

DATABASES = {
    'default': {
        'ENGINE': '',
        'NAME': '',
        'HOST': '',
        'USER': '',
        'PASSWORD': ''
    }
}

TIME_ZONE = None
LANGUAGE_CODE = 'en-us'
SITE_ID = 1
USE_I18N = False

MEDIA_ROOT = '/belleville/site_media'
MEDIA_URL = '/belleville/site_media'
ADMIN_MEDIA_PREFIX = '/media/'

TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.load_template_source',
    'django.template.loaders.app_directories.load_template_source',
)

MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'middleware.project_logging.LoggingMiddleware',
)

ROOT_URLCONF = 'belleville.urls'

TEMPLATE_ROOT = '/project/django'
TEMPLATE_DIRS = (
    '/project/belleville/templates/', 
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    'belleville.entry',
    'belleville.pages',
)

LOGOUT_URL = '/accounts/login/'

