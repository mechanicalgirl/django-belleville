import unittest, os

from django.test import Client, TestCase
from django.contrib.auth.models import User

from belleville.entry.models import Post
from belleville.entry.models import Category
from belleville.entry.models import PostCategory

MODELS = [Post, Category, PostCategory]

class BlogPostTestCase(unittest.TestCase):

    def setUp(self):
        """ Populate test database with model instances. """
        self.client = Client()

        self.user1 = User(username="bshaurette", first_name="Barbara", last_name="Shaurette")

        self.post1 = Post(author=self.user1, title='Test Post 1', slug='test-post-one', 
            body='Test Post Content', publish='t', created_at='2009-04-16', updated_at='2009-04-18')
        self.post2 = Post(author=self.user1, title='Test Post 2', slug='test-post-two',
            body='Test Post Content', publish='t', created_at='2009-04-16', updated_at='')

        self.category1 = Category(name='django', slug='django', created_at='2009-04-16', active='t')
        self.category2 = Category(name='vegan cooking', slug='vegan-cooking', created_at='2010-04-16', active='t')


    def test_post_str(self):
        self.assertEquals(Post.__str__(self.post1), 'Test Post 1')
        self.assertEquals(Post.__str__(self.post2), 'Test Post 2')

    def test_post_unicode(self):
        self.assertEquals(Post.__unicode__(self.post1), u'Test Post 1')
        self.assertEquals(Post.__unicode__(self.post2), u'Test Post 2')

    def test_post_get_absolute_url(self):
        self.assertEquals(Post.get_absolute_url(self.post1), '/post/test-post-one/')
        self.assertEquals(Post.get_absolute_url(self.post2), '/post/test-post-two/')

    def test_category_unicode(self):
        self.assertEquals(Category.__unicode__(self.category1), u'django')
        self.assertEquals(Category.__unicode__(self.category2), u'vegan cooking')


    def tearDown(self):
        """ Depopulate created model instances from test database. """
        for model in MODELS:
            for obj in model.objects.all():
                obj.delete()


