import logging

from django.conf import settings
from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from django.contrib.sites.models import Site
from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.template.loader import render_to_string

from belleville.entry.models import Post
from belleville.entry.models import Category
from belleville.entry.models import PostCategory

def list_all(request):
    """
    Returns a list of all posts, non-paginated, with title/author/date/category
    """
    template_name = 'all.html'
    context = {}
    list_all = Post.objects.filter(publish=True).order_by('-created_at')
    for entry in list_all:
        entry.category_list = Category.objects
                                      .filter(postcategory__post__pk=entry.id)
    context['entry_list'] = list_all
    return render_to_response(template_name, context, 
                              context_instance=RequestContext(request))

def list_entries(request, category=None, username=None, date=None):
    """
    List all blog posts, paginated
    """
    template_name = 'list.html'
    context = {}
    per_page = 5
    page = int(request.GET.get('page', '1'))

    if category:
        context['category'] = category
        grouped_list = entries_by_category(request, category)
    elif username:
        context['author'] = username
        try:
            user = User.objects.get(username=username)
            grouped_list = Post.objects.filter(author=user.id)
                               .order_by('-created_at')
        except ObjectDoesNotExist:
            grouped_list = None
    elif date:
        context['date'] = date
        grouped_list = Post.objects
                           .filter(created_at__startswith=date, publish=True)
                           .order_by('-created_at')
    else:
        grouped_list = Post.objects.filter(publish=True)
                                   .order_by('-created_at')

    for entry in grouped_list:
        entry.category_list = Category.objects
                                      .filter(postcategory__post__pk=entry.id)

    total_entries = grouped_list.count()
    total_pages = (total_entries/per_page)+1
    context['page_range'] = range(1, total_pages+1)

    offset = (page * per_page) - per_page
    limit = offset + per_page
    entry_list = grouped_list[offset:limit]
    context['entry_list'] = entry_list

    return render_to_response(template_name, context, 
                              context_instance=RequestContext(request))

def viewid(request, id):
    """
    View blog post by id
    """
    try:
        commented_entry = Post.objects.get(pk=id, publish=True)
        title = commented_entry.slug
        return HttpResponseRedirect('/view/%s/' % title)
    except ObjectDoesNotExist:
	return HttpResponseRedirect('/')

def view(request, title):
    """
    View a single blog post
    """
    template_name = 'view.html'
    context = {}

    try:
        entry = Post.objects.get(slug=title, publish=True)
    except ObjectDoesNotExist:
        entry = None

    if entry:
        try:
            entry.category_list = Category.objects
                                          .filter(postcategory__post__pk=entry.id)
            entry.body = str(entry.body)
        except ObjectDoesNotExist:
            entry.category_list = None

        context['entry'] = entry
        context['form'] = form

    return render_to_response(template_name, context, 
                              context_instance=RequestContext(request))

def preview(request, title):
    """
    Preview a blog post (superusers only)
    """
    template_name = 'view.html'
    context = {}

    try:
        entry = Post.objects.get(slug=title)
    except ObjectDoesNotExist:
        entry = None

    if entry:
        try:
            entry.category_list = Category.objects
                                          .filter(postcategory__post__pk=entry.id)
            entry.body = str(entry.body)
        except ObjectDoesNotExist:
            entry.category_list = None

        if request.method == 'POST':
            form = form_class(request.POST)
            if form.is_valid():
                form.post_id = entry.id
                form.save()
                return HttpResponseRedirect('/view/%s/' % title)
        else:
            form = form_class()

        context['entry'] = entry
        context['form'] = form

    return render_to_response(template_name, context, 
                              context_instance=RequestContext(request))


def entries_by_category(request, category):
    try:
        post_category = Category.objects.get(slug=category)
    except ObjectDoesNotExist:
        post_category = None
    if post_category: entry_list = Post.objects.filter(
                                       postcategory__category=post_category.id, 
                                       publish=True)
                                       .order_by('-created_at')
    return entry_list

